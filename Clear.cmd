@echo off
rem $MOD$ 000000 1000 Visual Studio 2010 Clear Script

SetLocal EnableExtensions

set extensions=*.suo
set directories=bin obj

set cmdDelete=del /A- /F /Q %extensions%
set cmdDeleteDirs=rmdir /S /Q %directories%

%cmdDeleteDirs% 2>nul
%cmdDelete% 2>nul

for /F "delims=" %%I in ('dir "%cd%" /AD /B /S') do (
	pushd %%I
	%cmdDeleteDirs% 2>nul
	%cmdDelete% 2>nul
	popd
)